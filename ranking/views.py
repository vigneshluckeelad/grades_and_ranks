import numpy
from django.shortcuts import render, render_to_response
from django.views.generic.base import TemplateView
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from scipy.stats import rankdata

from ranking.models import *


# Create your views here.
class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"
    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        if(self.request.user.is_superuser):
            context['students'] = Student.objects.all()
            context['subjects'] = Subject.objects.all() 
            context['all_marks'] = Mark.objects.all()
        else:
            context['students'] = Student.objects.all()
            context["subject"] = self.request.user.subject
            context["subject_id"] = self.request.user.subject.id
            context['marks'] = Mark.objects.filter(subject=self.request.user.subject)    
        return context
    
    
    def get(self, request, *args, **kwargs):
        if request.user.is_superuser:
            self.template_name = "admin-dashboard.html"
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


    def post(self, request, *args, **kwargs):
        for i in Student.objects.all():
            mark_value = request.POST.get("marks_"+ str(i.id), None)
            mark, created = Mark.objects.get_or_create(student=i, subject=self.request.user.subject)
            mark.value = mark_value
            mark.save()
            total_dict = Mark.objects.filter(student=i).aggregate(total = Sum('value'))
            total = total_dict['total']
            i.total = total
            i.save()
            total_list = Student.objects.values_list('total', flat=True)
            total_numpy = numpy.array(total_list)
            rank_list_np = rankdata(total_numpy, method='min')
            rank_list = rank_list_np.tolist()
            value_dict = {}
        for tot, rnk in zip(total_list, rankdata(total_numpy, method='min').tolist()):
            value_dict[tot] = rnk
            for student in Student.objects.all():
                if student.total == tot:
                    student.rank = value_dict[tot]
                    student.save()
        return redirect(reverse('success'))
