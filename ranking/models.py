# Import Statements
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils import timezone
from django.urls import reverse_lazy, reverse
from django.core.exceptions import ObjectDoesNotExist


# Model classes
class Subject(models.Model):
    subject = models.CharField(max_length=250)
    subject_code = models.IntegerField(unique=True)

    def __str__(self):
        '''To represent object'''
        return '{}-{}'.format(self.subject, self.subject_code)



class Teacher(AbstractUser):
    subject = models.OneToOneField(Subject, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        '''To represent object'''
        return '{} of {}'.format(self.first_name, self.subject)


class Student(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    total = models.FloatField(null=True, blank=True)
    rank = models.IntegerField(null=True, blank=True)

    def __str__(self):
        '''To represent object'''
        return '{}'.format(self.name)


class Mark(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    value = models.IntegerField(null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        '''To represent object'''
        return '{} for {}'.format(self.subject, self.student)

