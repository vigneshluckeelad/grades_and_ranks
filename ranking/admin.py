from django.contrib import admin

# Register your models here.
from django.utils.translation import ugettext_lazy as _
from ranking.models import Teacher,Subject,Student,Mark
from django.contrib.auth.admin import UserAdmin


UserAdmin.fieldsets = (
    (None, {'fields': ('username', 'password')}),
    (_('Subject info'), {'fields': ('subject',)}),
    (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
)

admin.site.register(Teacher, UserAdmin)
admin.site.register(Subject)
admin.site.register(Student)
admin.site.register(Mark)
