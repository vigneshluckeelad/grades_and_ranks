from django import template
from django.conf import settings
from ranking.models import *
register = template.Library()


@register.filter(name='marks')
def marks(student_id, subject_id):
    if Mark.objects.filter(student_id=student_id, subject_id=subject_id).exists():
        mark = Mark.objects.filter(student_id=student_id, subject_id=subject_id)[0]
        marks = mark.value
    else:
        marks = None    
    return marks